#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "lists/list.h"
#include "sorting/bubble.h"
#include "sorting/cocktail.h"
#include "sorting/comb.h"
#include "sorting/gnome.h"
#include "sorting/heap.h"
#include "sorting/insertion.h"
#include "sorting/quick.h"
#include "sorting/selection.h"
#include "sorting/shell.h"
#include "sorting/stooge.h"

#define LIST_SIZE 100
#define MAX_VALUE 85

/**
 * Prints the value of the item to the console
 *
 * @param item The item to be printed
 */
void item_print(item_t *item) {
	printf("%d ", *(int*) item);
}

/**
 * Prints the size and contents of the list to the console
 *
 * @param list The list to be printed
 */
void list_print(list_t *list) {
	printf("%lu [ ", list_size(list));
	list_iterate(list, &item_print);
	printf("]\n");
}

/**
 * Compares a the pair of items with each other returning true if the value of A
 * is greater than the value of B.
 *
 * @param  a The first item to be compared
 * @param  b The second item to be compared
 * @return True if A < B, else false
 */
int compare(item_t *a, item_t *b) {
	return *(int*) a < *(int*) b;
}

int main() {
	srand((unsigned) time(NULL));
	int array[LIST_SIZE];
	char pick;

	do {
		printf("\nSelect a sorting algorithm to measure:\n\n");
		printf("0. Bubble sort\n");
		printf("1. Cocktail shaker sort\n");
		printf("2. Comb sort\n");
		printf("3. Gnome sort\n");
		printf("4. Heapsort\n");
		printf("5. Insertion sort\n");
		printf("6. Quicksort\n");
		printf("7. Selection sort\n");
		printf("8. Shellsort\n");
		printf("9. Stooge sort\n");
		printf("Q. Quit\n");
		printf("\n> ");

		scanf("%c", &pick);
		if (pick == 'q' || pick == 'Q') {
			return 0;
		}
	} while(pick < '0' || pick > '9');

	list_t* list = list_create();
	struct timespec a, b;

	// Fill list with random values
	for (size_t i = 0; i < LIST_SIZE; i++) {
		array[i] = rand() % MAX_VALUE;
		list_insert(list, &array[i], 0);
	}

	// Print list with randomly arranged values
	printf("\nBefore sorting: ");
	list_print(list);

	clock_gettime(CLOCK_REALTIME, &a);

	// Sort list
	switch (pick) {
		case '0':
			bubble_sort(list, compare);
			break;
		case '1':
			cocktail_sort(list, compare);
			break;
		case '2':
			comb_sort(list, compare);
			break;
		case '3':
			gnome_sort(list, compare);
			break;
		case '4':
			heap_sort(list, compare);
			break;
		case '5':
			insertion_sort(list, compare);
			break;
		case '6':
			quick_sort(list, compare);
			break;
		case '7':
			selection_sort(list, compare);
			break;
		case '8':
			shell_sort(list, compare);
			break;
		case '9':
			stooge_sort(list, compare);
	}

	clock_gettime(CLOCK_REALTIME, &b);

	// Print list with sorted values
	printf("\nAfter sorting: ");
	list_print(list);

	// Print time taken
	printf("\nTime (s): %ld.%ld\n", b.tv_sec - a.tv_sec, b.tv_nsec - a.tv_nsec);

	list_destroy(&list);

	return 0;
}
