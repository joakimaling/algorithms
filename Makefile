CFLAGS = -Wall -Wextra -pedantic
CPPFLAGS = -MMD -MP

.PHONY: clean run

# Link object into target
main: main.o
	$(CC) -o $@ $^

# Include dependency
-include main.d

# Compile source code
main.o: main.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

# Remove dependency, object & target
clean:
	$(RM) main *.d *.o

# Run target
run: main
	./main
