#ifndef COMB_H
#define COMB_H

#include <math.h>
#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the comb sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Comb_sort
 *
 * @param list    The list of values to be sorted
 * @param compare A callback instructing how to arrange the list items
 */
void comb_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t done, gap = list_size(list), size = gap;
	item_t **a, **b;

	do {
		if ((gap = floor(gap / 1.3)) > 1) {
			done = 0;
		}
		else {
			done = 1;
			gap = 1;
		}
		for (size_t i = 0; i + gap < size; i++) {
			a = list_get_item(list, i + gap);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}
	} while (!done);
}

#endif
