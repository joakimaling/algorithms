#ifndef ODDEVEN_H
#define ODDEVEN_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the odd-even sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Odd-even_sort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void oddeven_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t done, size = list_size(list);
	item_t **a, **b;

	do {
		done = 1;
		for (size_t i = 1; i < size - 1; i += 2) {
			a = list_get_item(list, i + 1);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}

		for (size_t i = 0; i < size - 1; i += 2) {
			a = list_get_item(list, i + 1);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}
	} while(!done);
}

#endif
