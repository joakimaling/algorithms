#ifndef BUBBLE_H
#define BUBBLE_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the bubble sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Bubble_sort
 *
 * @param list    The list of values to be sorted
 * @param compare A callback instructing how to arrange the list items
 */
void bubble_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t done, size = list_size(list);
	item_t **a, **b;

	do {
		done = 1;
		for (size_t i = 0; i < size - 1; i++) {
			a = list_get_item(list, i + 1);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}
		size--;
	} while (!done);
}

#endif
