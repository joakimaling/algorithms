#ifndef COCKTAIL_H
#define COCKTAIL_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the cocktail shaker sort algorithm. The callback
 * function 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Cocktail_shaker_sort
 *
 * @param list    The list of values to be sorted
 * @param compare A callback instructing how to arrange the list items
 */
void cocktail_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t done, size = list_size(list);
	item_t **a, **b;

	do {
		done = 1;
		for (size_t i = 0; i < size - 2; i++) {
			a = list_get_item(list, i + 1);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}
		if (done) {
			break;
		}
		for (size_t i = size - 2; i > 0; i--) {
			a = list_get_item(list, i + 1);
			b = list_get_item(list, i);

			if (compare(*a, *b)) {
				swap(a, b);
				done = 0;
			}
		}
	} while (!done);
}

#endif
