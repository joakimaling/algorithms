#ifndef HEAP_H
#define HEAP_H

#include <math.h>
#include "../lists/list.h"
#include "swap.h"

/**
 * Repeatedly moves the largest item from the root of the heap to the end of the
 * list
 *
 * @param list    List of values to be sifted down
 * @param compare Callback instructing how to arrange the list items
 */
void sift_down(list_t* list, int (*compare)(item_t*, item_t*), size_t p, size_t q) {
	size_t child, root = p, swp;
	item_t **a, **b;

	while (2 * root + 1 <= q) {
		child = 2 * root + 1;
		swp = root;

		a = list_get_item(list, swp);
		b = list_get_item(list, child);

		if (compare(*a, *b)) {
			swp = child;
		}

		a = list_get_item(list, swp);
		b = list_get_item(list, child + 1);

		if (child + 1 <= q && compare(*a, *b)) {
			swp = child + 1;
			a = list_get_item(list, swp);
		}

		if (swp == root) {
			return;
		}
		else {
			a = list_get_item(list, root);
			b = list_get_item(list, swp);

			swap(a, b);
			root = swp;
		}
	}
}

/**
 * Constructs a heap out of the given list by creating the layout of a complete
 * binary tree which maps the structure into the list items; each item is a node
 *
 * @param list    List of values to be heapified
 * @param compare Callback instructing how to arrange the list items
 */
void heapify(list_t* list, int (*compare)(item_t*, item_t*)) {
	size_t size = list_size(list) - 1;

	for (ssize_t i = floor((double) (size - 1) / 2); i >= 0; i--) {
		sift_down(list, compare, i, size);
	}
}

/**
 * Sorts a given list using the heapsort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items.
 *
 * https://en.wikipedia.org/wiki/Heapsort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void heap_sort(list_t* list, int (*compare)(item_t*, item_t*)) {
	item_t **a, **b;

	heapify(list, compare);

	a = list_get_item(list, 0);

	for (size_t i = list_size(list) - 1; i > 0;) {
		b = list_get_item(list, i);

		swap(a, b);
		sift_down(list, compare, 0, --i);

	}
}

#endif
