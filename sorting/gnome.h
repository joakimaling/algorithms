#ifndef GNOME_H
#define GNOME_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the gnome sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Gnome_sort
 *
 * @param list    The list of values to be sorted
 * @param compare A callback instructing how to arrange the list items
 */
void gnome_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t i = 0, size = list_size(list);
	item_t **a, **b;

	while (i < size) {
		a = list_get_item(list, i);
		b = list_get_item(list, i - 1);

		if (i == 0 || !compare(*a, *b)) {
			i++;
		} else {
			swap(a, b);
			i--;
		}
	}
}

#endif
