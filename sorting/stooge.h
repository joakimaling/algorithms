#ifndef STOOGE_H
#define STOOGE_H

#include "../lists/list.h"
#include "swap.h"

/**
 * This is a helper function which deals with the recursion part of the
 * stooge sort algorithm. It helps keeping the interface function clean
 *
 * @param list    The list to sort
 * @param compare A callback instructing how to arrange the list items
 * @param p       The low pivot from a previous recursion
 * @param q       The high pivot from a previous recursion
 */
void stooge(list_t *list, int (*compare)(item_t*, item_t*), size_t i, size_t j) {
	item_t **a = list_get_item(list, j);
	item_t **b = list_get_item(list, i);
	size_t t;

	if (compare(*a, *b)) {
		swap(a, b);
	}

	if (j - i + 1 > 2) {
		t = (j - i + 1) / 3;
		stooge(list, compare, i, j - t);
		stooge(list, compare, i + t, j);
		stooge(list, compare, i, j - t);
	}
}

/**
 * Sorts a given list using the stooge sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Bubble_sort
 *
 * @param list    The list of values to be sorted
 * @param compare A callback instructing how to arrange the list items
 */
void stooge_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	stooge(list, compare, 0, list_size(list) - 1);
}

#endif
