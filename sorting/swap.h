#ifndef SWAP_H
#define SWAP_H

#include "../lists/list.h"

/**
 * Swaps the positions of two list items. This funcition is used by the sorting
 * algorithms
 *
 * @param a The first list item
 * @param b Th second list item
 */
void swap(item_t **a, item_t **b) {
	item_t *c = *a;
	*a = *b;
	*b = c;
}

#endif
