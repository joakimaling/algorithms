#ifndef SELECTION_H
#define SELECTION_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the selection sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Selection_sort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void selection_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t min, size = list_size(list);
	item_t **a, **b;

	for (size_t i = 0; i < size - 1; i++) {
		min = i;
		for (size_t j = i + 1; j < size; j++) {
			a = list_get_item(list, j);
			b = list_get_item(list, min);

			if (compare(*a, *b)) {
				min = j;
			}
		}
		if (min != i) {
			swap(list_get_item(list, i), list_get_item(list, min));
		}
	}
}

#endif
