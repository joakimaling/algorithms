#ifndef INSERTION_H
#define INSERTION_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the insertion sort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Insertion_sort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void insertion_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t j, size = list_size(list);
	item_t **a, **b;

	for (size_t i = 1; i < size; i++) {
		j = i;
		while (j > 0) {
			a = list_get_item(list, j);
			b = list_get_item(list, j - 1);

			if (compare(*a, *b)) {
				swap(a, b);
				j--;
			}
			else {
				break;
			}
		}
	}
}

#endif
