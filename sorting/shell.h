#ifndef SHELL_H
#define SHELL_H

#include "../lists/list.h"
#include "swap.h"

/**
 * Sorts a given list using the Shellsort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Shellsort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void shell_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	size_t gap = list_size(list) / 2, j, size = list_size(list);
	item_t **a, **b;

	while (gap > 0) {
		for (size_t i = gap; i < size; i++) {
			j = i;
			while (j > 0) {
				a = list_get_item(list, j);
				b = list_get_item(list, j - 1);

				if (compare(*a, *b)) {
					swap(a, b);
					j--;
				}
				else {
					break;
				}
			}
		}
		gap /= 2;
	}
}

#endif
