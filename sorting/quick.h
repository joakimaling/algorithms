#ifndef QUICK_H
#define QUICK_H

#include "../lists/list.h"
#include "swap.h"

/**
 * This function uses Hoare's partitioning scheme which reorders the list items
 * so that all items with values less than the pivot come before the pivot,
 * while all elements with values greater than the pivot come after it. After
 * this partitioning, the pivot is in its final position
 *
 * @param  list    The list to partition
 * @param  compare A callback instructing how to arrange the list items
 * @param  p       The low pivot from a previous recursion
 * @param  q       The high pivot from a previous recursion
 * @return         A number representiong the new pivot
 */
size_t hoare(list_t *list, int (*compare)(item_t*, item_t*), size_t p, size_t q) {
	size_t i = p - 1, j = q + 1;
	item_t **a, **b, **pivot = list_get_item(list, p);

	while (1) {
		do {
			a = list_get_item(list, ++i);
		} while(compare(*a, *pivot));

		do {
			b = list_get_item(list, --j);
		} while(compare(*pivot, *b));

		if (i >= j) {
			return j;
		}

		swap(a, b);
	}
}

/**
 * This is a helper function which deals with the recursion part of the
 * quicksort algorithm. It helps keeping the interface function clean
 *
 * @param list    The list to sort
 * @param compare A callback instructing how to arrange the list items
 */
void quick(list_t *list, int (*compare)(item_t*, item_t*), size_t p, size_t q) {
	if (p < q) {
		size_t pivot = hoare(list, compare, p, q);
		quick(list, compare, p, pivot);
		quick(list, compare, pivot + 1, q);
	}
}

/**
 * Sorts a given list using the quicksort algorithm. The callback function
 * 'compare' instructs the algorithm how to arrange the list items
 *
 * https://en.wikipedia.org/wiki/Quicksort
 *
 * @param list    List of values to be sorted
 * @param compare Callback instructing how to arrange the list items
 */
void quick_sort(list_t *list, int (*compare)(item_t*, item_t*)) {
	quick(list, compare, 0, list_size(list) - 1);
}

#endif
