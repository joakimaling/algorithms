## Unreleased
- Added the first version of the Odd-even sort algorithm

## 0.1.1 (2018-09-25)
- Added the **Heapsort** algorithm and the **Stooge sort** algorithm
- Changed the test menu to accommodate the new algorithms
- Improved the structure of the **Makefile** and the comments in all the files

## 0.1.0 (2018-09-05)
- Initial public release
