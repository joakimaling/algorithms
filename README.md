# Algorithms

> Small collection of data structures and algorithms

[![Licence][licence-badge]][licence-url]
[![Release][release-badge]][release-url]

This project contains a collection of different data structures and algorithms
I've collected over a few years, mostly from different courses at school. I've
reworked them to refresh my understanding of C and pushed them to GitHub hoping
someone might find them useful.

## Installation

Add it to your project through cloning or as a submodule:

```sh
git clone http://github.com/joakimaling/algorithms.git
# or
git submodule add http://github.com/joakimaling/algorithms.git
```

You can test the algorithms through the provided `main.c` file. Just run this to
compile and run the code:

```sh
make && ./main
```

## Usage

Simply include them into your C/C++ code like this. For example if you want to
use the *bubble sort algorithm*:

```c
#include "algorithms/sorting/bubble.h"
```

## To-Do list

See [TODO.md](TODO.md) for upcoming changes.

## Licence

Released under GNU-3.0. See [LICENSE][licence-url] for more.

Coded with :heart: by [joakimaling][user-url].

[licence-badge]: https://img.shields.io/github/license/joakimaling/algorithms.svg
[licence-url]: LICENSE
[release-badge]: https://img.shields.io/github/release/joakimaling/algorithms.svg
[release-url]: https://github.com/joakimaling/algorithms/releases
[user-url]: https://github.com/joakimaling
