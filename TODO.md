# algorithms

- [ ] improve cocktail sort
- [ ] improve odd-even sort (support for multiple processors/cores)
- [ ] improve comments for heap sort
- [ ] add assert to linked list - running insert before create...
- [ ] move from using index to iterator
- [x] add copy/clone function to list, queue and stack
- [ ] add merge function to queue
- [ ] add merge function to stack
- [ ] fix and test the tree
- [ ] fix and test the hash
- [ ] fix and test the heap
- [ ] fix and test the set
