#ifndef STACK_H
#define STACK_H

#include "list.h"

typedef list_t stack_t;

/**
 * Creates a new stack. If the memory allocation failed null is returned
 *
 * @return A pointer to the new stack, or null
 */
stack_t *stack_create() {
	return (stack_t*) list_create();
}

/**
 * Destroys a stack, if not already null. It clears all its nodes before freeing
 * up its own memory
 *
 * @param stack The stack to be destroyed
 */
void stack_destroy(stack_t **stack) {
	list_destroy((list_t**) stack);
}

/**
 * Returns the size (the number of nodes it contains) of a stack. If the stack
 * is null it returns a zero
 *
 * @param  stack The stack to check
 * @return       A number representing the size of the stack
 */
size_t stack_size(stack_t *stack) {
	return list_size((list_t*) stack);
}

/**
 * Returns true if a stack is empty, otherwise false
 *
 * @param  stack The stack to check
 * @return       True if empty, else false
 */
int stack_is_empty(stack_t *stack) {
	return list_is_empty((list_t*) stack);
}

/**
 * Pushes an item to the top of a stack
 *
 * @param stack The stack on top of which to push the item
 * @param item  The item to push to the top of the stack
 */
void stack_push(stack_t *stack, item_t *item) {
	list_insert((list_t*) stack, item, 0);
}

/**
 * Removes and returns the top item from a stack
 *
 * @param  stack The stack from which to pop the item
 * @return       A pointer to the popped item
 */
item_t* stack_pop(stack_t *stack) {
	return (stack_t*) list_remove((list_t*) stack, 0);
}

/**
 * Iterates over a stack applying the callback function to every item within it
 *
 * @param stack    The stack to iterate over
 * @param callback A callback to apply to each item
 */
void stack_iterate(stack_t *stack, void (*callback)(item_t *item)) {
	list_iterate((list_t*) stack, callback);
}

#endif
