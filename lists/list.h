#ifndef LIST_H
#define LIST_H

#include <malloc.h>

typedef void item_t;

typedef struct node_t {
	struct node_t *previous, *next;
	item_t *item;
} node_t;

typedef struct list_t {
	struct node_t *head, *tail;
	size_t size;
} list_t;

/**
 * Creates a new list. If the memory allocation failed null is returned
 *
 * @return A pointer to the new list, or null
 */
list_t *list_create() {
	list_t *list = (list_t*) malloc(sizeof(list_t));

	if (list) {
		list->head = NULL;
		list->tail = NULL;
		list->size = 0;
	}

	return list;
}

/**
 * Destroys a list, if not already null. It clears all its nodes before freeing
 * up its own memory
 *
 * @param list The list to be destroyed
 */
void list_destroy(list_t **list) {
	if (list) {
		node_t *next, *node = (*list)->head;

		while (node) {
			next = node->next;
			free(node);
			node = next;
		}

		free(*list);
		*list = NULL;
	}
}

/**
 * Returns the size (the number of nodes it contains) of a list. If the list is
 * null it returns a zero
 *
 * @param  list The list to check
 * @return      A number representing the size of the list
 */
size_t list_size(list_t *list) {
	return !list ? 0 : list->size;
}

/**
 * Returns true if a list is empty, otherwise false
 *
 * @param  list The list to check
 * @return      True if empty, else false
 */
int list_is_empty(list_t *list) {
	return list_size(list) == 0;
}

/**
 * Inserts a new node into a list at an index. If the index is greater than the
 * size of the list the node is added to the end of the list.
 *
 * @param list  The list to be extended
 * @param item  The item to be inserted
 * @param index The index where the item will be inserted
 */
void list_insert(list_t *list, item_t *item, size_t index) {
	node_t *node = (node_t*) malloc(sizeof(node_t));

	if (node) {
		if (list_is_empty(list)) {
			node->previous = NULL;
			node->next = NULL;
			list->head = node;
			list->tail = node;
		}
		else {
			if (index >= list_size(list)) {
				list->tail->next = node;
				node->previous = list->tail;
				node->next = NULL;
				list->tail = node;
			}
			else {
				node_t *at = list->head;
				size_t current = 0;

				while (at) {
					if (current++ == index) {
						if (at == list->head) {
							node->previous = NULL;
							list->head = node;
						}
						else {
							node->previous = at->previous;
							at->previous->next = node;
						}

						at->previous = node;
						node->next = at;
						break;
					}

					at = at->next;
				}
			}
		}

		node->item = item;
		list->size++;
	}
}

/**
 * Removes a node at given index in a list and returns its item
 *
 * @param  list  The list to be manipulated
 * @param  index The index of which
 * @return       The item, or null
 */
item_t *list_remove(list_t *list, size_t index) {
	if (!list_is_empty(list)) {
		node_t *at = list->head;
		size_t current = 0;

		while (at != NULL) {
			if (current++ == index) {
				item_t *item = at->item;

				if (list_size(list) == 1) {
					list->head = NULL;
					list->tail = NULL;
				}
				else if (at->previous == NULL) {
					at->next->previous = NULL;
					list->head = at->next;
				}
				else if (at->next == NULL) {
					at->previous->next = NULL;
					list->tail = at->previous;
				}
				else {
					at->next->previous = at->previous;
					at->previous->next = at->next;
				}

				list->size--;
				free(at);

				return item;
			}
			at = at->next;
		}
	}

	return NULL;
}

/**
 * Returns the address of the item at the index in a list.
 *
 * @param  list  The list to go through
 * @param  index The index of the item
 * @return       A pointer to the item or null
 */
item_t **list_get_item(list_t *list, size_t index) {
	node_t *node = list->head;
	size_t current = 0;

	while (node) {
		if (current++ == index) {
			return &node->item;
		}

		node = node->next;
	}

	return NULL;
}

/**
 * Iterates over a list applying the callback function to every item within it
 *
 * @param list     The list to iterate over
 * @param callback A callback to apply to each item
 */
void list_iterate(list_t *list, void (*callback)(item_t *item)) {
	if (list) {
		node_t *node = list->head;

		while (node) {
			callback(node->item);
			node = node->next;
		}
	}
}

/**
 * Merges two lists into one. The resulting list will be stored in list A and
 * list B will become null
 *
 * @param a The first list
 * @param b The second list
 */
void list_merge(list_t **a, list_t **b) {
	if (!list_is_empty(*b)) {
		if (list_is_empty(*a)) {
			(*a)->head = (*b)->head;
		}
		else {
			(*a)->tail->next = (*b)->head;
			(*b)->head->previous = (*a)->tail;
		}

		(*a)->tail = (*b)->tail;
		(*a)->size += (*b)->size;
		free(*b);
		*b = NULL;
	}
}

#endif
