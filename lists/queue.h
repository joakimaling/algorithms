#ifndef QUEUE_H
#define QUEUE_H

#include "list.h"

typedef list_t queue_t;

/**
 * Creates a new queue. If the memory allocation failed null is returned
 *
 * @return A pointer to the new queue, or null
 */
queue_t *queue_create() {
	return (queue_t*) list_create();
}

/**
 * Destroys a queue, if not already null. It clears all its nodes before freeing
 * up its own memory
 *
 * @param queue The queue to be destroyed
 */
void queue_destroy(queue_t **queue) {
	list_destroy((list_t**) queue);
}

/**
 * Returns the size (the number of nodes it contains) of a queue. If the queue
 * is null it returns a zero
 *
 * @param  queue The queue to check
 * @return       A number representing the size of the queue
 */
size_t queue_size(queue_t *queue) {
	return list_size((list_t*) queue);
}

/**
 * Returns true if a queue is empty, otherwise false
 *
 * @param  queue The queue to check
 * @return       True if empty, else false
 */
int queue_is_empty(queue_t *queue) {
	return list_is_empty((list_t*) queue);
}

/**
 * Pushes an item to the back of a queue
 *
 * @param queue The queue at which back to push the item
 * @param item  The item to push to the back of the queue
 */
void queue_push(queue_t *queue, item_t *item) {
	list_insert((list_t*) queue, item, list_size((list_t*) queue));
}

/**
 * Removes and returns the front item from a queue
 *
 * @param  queue The queue from which front pop the item
 * @return       A pointer to the popped item
 */
item_t* queue_pop(queue_t *queue) {
	return (queue_t*) list_remove((list_t*) queue, 0);
}

/**
 * Iterates over a queue applying the callback function to every item within it
 *
 * @param queue    The queue to iterate over
 * @param callback A callback to apply to each item
 */
void queue_iterate(queue_t *queue, void (*callback)(item_t *item)) {
	list_iterate((list_t*) queue, callback);
}

#endif
